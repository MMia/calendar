import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Calendar {

    public List<LocalDate> getDatesFromGivenPeriodWithoutWeekends(LocalDate from, LocalDate to) {

        return Stream.iterate(from, date -> date.plusDays(1))
                .limit(ChronoUnit.DAYS.between(from, to))
                .filter(date -> date.getDayOfWeek() != DayOfWeek.SUNDAY && date.getDayOfWeek() != DayOfWeek.SATURDAY)
                .peek(System.out::println)
                .collect(Collectors.toList());
    }
}

