import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Calendar calendar = new Calendar();
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Type start date in format: yyyyMMdd");
            String startDate = sc.nextLine();
            LocalDate fromDate = LocalDate.parse(startDate, DateTimeFormatter.ofPattern("yyyyMMdd", Locale.US));
            System.out.println("Type end date in format: yyyyMMdd");
            String endDate = sc.nextLine();
            LocalDate toDate = LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyyMMdd", Locale.US));

            calendar.getDatesFromGivenPeriodWithoutWeekends(fromDate, toDate);
        }
    }
}
